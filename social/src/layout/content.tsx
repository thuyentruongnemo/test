import React from "react";
import SideBar from "../components/side-bar/index.tsx";
import Navigator from "../components/navigator/index.tsx";
import TodayTrending from "../components/today-trending/index.tsx";
import {Outlet} from "react-router-dom";

const Content = () => {

    return (
        <div className="w-full h-full">
            <div className="flex w-full h-full">
                <div className="w-1/5 border-r-[1px] border-[#26292e]">
                    <SideBar/>
                </div>
                <div className="w-4/5">
                    <div className="h-[10%] border-b-[1px] border-[#26292e]">
                        <Navigator/>
                    </div>
                    <div className="flex h-[90%]">
                        <Outlet/>
                        <div className="w-1/3 h-[80%]">
                            <TodayTrending/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default React.memo(Content);