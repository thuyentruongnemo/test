import Content from './layout/content';
import './App.css';
import {
    createBrowserRouter, Navigate,
    RouterProvider,
} from "react-router-dom";
import React from "react";
import SignInPage from "./components/log-in";
import {AuthContext} from "./context/authContext.tsx";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import ProfilePage from "./components/profile-page";
import MainSection from "./components/main-section";

function App() {

    const queryClient = new QueryClient()

    const { currentUser } = React.useContext(AuthContext)

    const ProtectedRoute = ({ children }: { children: React.ReactNode }) => {
        if (!currentUser) {
            return <Navigate to="/login" />;
        }

        return children;
    };

    const router = createBrowserRouter([
        {
            path: "/",
            element: <SignInPage/>,
        },
        {
            path: "/",
            element: (
                <ProtectedRoute>
                    <Content/>
                </ProtectedRoute>
            ),
            children: [
                {
                    path: "/home",
                    element: <MainSection />,
                },
                {
                    path: "/profile/:id",
                    element: <ProfilePage />,
                },
            ]
        },
    ]);

    return (
        <QueryClientProvider client={queryClient}>
            <div className='h-screen bg-[#17181c]'>
                <RouterProvider router={router}/>
            </div>
        </QueryClientProvider>
    )
}

export default React.memo(App)
