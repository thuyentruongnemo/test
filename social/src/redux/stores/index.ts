import { configureStore } from "@reduxjs/toolkit";
import postReducer from "../reducers/post-reducer.ts";
import getUserReducer from "../reducers/get-user.ts";

export const store = configureStore({
    reducer: {
        postManager: postReducer,
        getUser: getUserReducer,
    },
})

export type RootState = ReturnType<typeof store.getState>