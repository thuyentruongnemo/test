import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { Post } from "../../types/type";

const initialState: Post[] = []

export const postReducer = createSlice({
    name: 'postReducer',
    initialState,
    reducers: {
        addPost: (state, action: PayloadAction<Post>) => {
            state.push(action.payload)
        },
    },
})

export const { addPost } = postReducer.actions

export default postReducer.reducer