import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

const initialState = {
    statusNeedToRefresh: false,
    id: -9999,
};

export const getUserReducer = createSlice({
    name: 'postReducer',
    initialState,
    reducers: {
        getUser: (state, action: PayloadAction<any>) => {
            state.statusNeedToRefresh = !state.statusNeedToRefresh
            state.id = action.payload.id
        },
    },
})

export const { getUser } = getUserReducer.actions

export default getUserReducer.reducer