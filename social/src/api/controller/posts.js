import { db } from "../connect.js"
import moment from "moment";

export const getPosts = async (req, res) => {

    const userId = req.query.userId;

    const q =
        userId === undefined
        ? `
            SELECT p.*, u.name, u.profilePicture
            FROM posts AS p 
            JOIN users AS u ON p.userId = u.id 
            GROUP BY p.id
        `
        : `
            SELECT p.*, u.name, u.profilePicture
            FROM posts AS p
            JOIN users AS u ON p.userId = u.id
            WHERE p.userId = ?
            GROUP BY p.id
        `
    ;
    db.query(q, [userId], (err, result) => {
        if (err) {
            res.status(404).json({ message: err.message });
        } else {
            res.status(200).json(result);
        }
    });
}

export const addPost = async (req, res) => {
    const q = "INSERT INTO posts (`caption`, `img`, `createdAt`, `userId`) VALUES (?, ?, ?, ?)";
    db.query(
        q,
        [req.body.caption, req.body.img, moment(Date.now()).format("YYYY-MM-DD HH:mm:ss"), req.body.userId],
        (err, _result) => {
            if (err) {
                res.status(409).json({ message: err.message });
            } else {
                res.status(200).json({ message: "add post success" });
            }
        }
    );
}

export const removePost = async (req, res) => {
    const q = "DELETE FROM posts WHERE id = ?";
    db.query(q, [req.body.id], (err, _result) => {
        if (err) {
            res.status(409).json({ message: err.message });
        } else {
            res.status(200).json({ message: "remove post success" });
        }
    });
}

export const updatePost = async (req, res) => {
    const q = "UPDATE posts SET caption = ?, img = ? WHERE id = ?";
    db.query(q, [req.body.caption, req.body.img, req.body.id], (err, _result) => {
        if (err) {
            res.status(409).json({ message: err.message });
        } else {
            res.status(200).json({ message: "update post success" });
        }
    });
}