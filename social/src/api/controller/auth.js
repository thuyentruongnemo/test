import { db } from "../connect.js";

export const register = (req, res) => {
    const checkUserQuery = "SELECT * FROM users WHERE username = ?";

    db.query(checkUserQuery, [req.body.username], (err, result) => {
        if (err) {
            return res.status(500).json({message: err});
        } else if (result.length) {
            return res.status(409).json({message: "User already exists"});
        } else {
            const insertUserQuery = "INSERT INTO users (`username`, `email`, `password`, `name`) VALUES (?, ?, ?, ?)";

            db.query(
                insertUserQuery,
                [req.body.username, req.body.email, req.body.password, req.body.name],
                (err, _result) => {
                    if (err) {
                        return res.status(500).json({message: err});
                    }
                    return res.status(200).json({message: "User created"});
                }
            );
        }
    });
}

export const login = (req, res) => {
    const checkUserQuery = "SELECT * FROM users WHERE username = ? AND password = ?";

    db.query(checkUserQuery, [req.body.username, req.body.password], (err, result) => {
        if (err) {
            return res.status(500).json({message: err});
        }
        if (!result.length) {
            return res.status(409).json({message: "Username or password is incorrect"});
        }
        return res.status(200).json(result[0]);
    });
}

export const logout = (req, res) => {
    return res.status(200).json({message: "Logged out"});
}