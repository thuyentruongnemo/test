import { db } from '../connect.js';

export const getComments = async (req, res) => {
    const q = "SELECT comments.*, users.name, users.profilePicture FROM comments JOIN users ON comments.userId = users.id WHERE postId = ? ORDER BY comments.createdAt DESC";

    db.query(q, [req.query.postId], (err, result) => {
        if (err) {
            res.status(400).json({ message: "Error fetching comments" });
        } else {
            res.status(200).json(result);
        }
    })
};

export const addComment = async (req, res) => {
    const q = "INSERT INTO comments (`postId`, `userId`, `comment`) VALUES (?, ?, ?)";

    db.query(q, [req.body.postId, req.body.userId, req.body.comment], (err, _result) => {
        if (err) {
            res.status(400).json({ message: "Error adding comment" });
        } else {
            res.status(200).json({ message: "Comment added" });
        }
    });
};