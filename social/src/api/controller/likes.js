import { db } from '../connect.js';

export const getLikes = async (req, res) => {
    const q = "SELECT COUNT(*) as totalLikes FROM likes WHERE postId = ?";
    db.query(q, [req.query.postId], (err, result) => {
        if (err) {
            res.status(400).json({ message: "Error fetching likes" });
        } else {
            res.status(200).json(result[0].totalLikes);
        }
    });
}

export const addLike = async (req, res) => {
    const q = "INSERT INTO likes (`postId`, `userId`) VALUES (?, ?)";
    db.query(q, [req.body.postId, req.body.userId], (err, _result) => {
        if (err) {
            res.status(400).json({ message: "Error adding like" });
        } else {
            res.status(200).json({ message: "Like added" });
        }
    });
}

export const removeLike = async (req, res) => {
    const q = "DELETE FROM likes WHERE postId = ? AND userId = ?";
    db.query(q, [req.body.postId, req.body.userId], (err, _result) => {
        if (err) {
            res.status(400).json({ message: "Error removing like" });
        } else {
            res.status(200).json({ message: "Like removed" });
        }
    });
}