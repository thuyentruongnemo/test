import { db } from '../connect.js'

export const getUsers = async (req, res) => {
    const userId = req.params.userId;
    const q = "SELECT * FROM users WHERE id=?";

    db.query(q, [userId], (err, data) => {
        if (err) return res.status(500).json(err);
        return res.json(data[0]);
    });
}

export const updateUser = (req, res) => {
    const q =
        "UPDATE users SET `name`=?,`city`=?,`username`=?,`email`=?,`password`=?,`profilePicture`=?,`coverPicture`=? WHERE id=?";

    db.query(
        q,
        [
            req.body.name,
            req.body.city,
            req.body.username,
            req.body.email,
            req.body.password,
            req.body.profilePicture,
            req.body.coverPicture,
            req.body.id,
        ],
        (err, data) => {
            if (err) return res.status(409).json(err);
            if (data.affectedRows > 0) return res.json("Updated successfully!");
            return res.status(403).json("You can update only your post!");
        }
    );
};
