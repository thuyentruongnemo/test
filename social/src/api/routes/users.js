import express from 'express';

const router = express.Router();

import { getUsers, updateUser } from '../controller/users.js';

router.get("/find/:userId", getUsers);
router.post("/updateUser", updateUser);
export default router;