import express from 'express';

const router = express.Router();

import { getPosts, addPost, removePost, updatePost } from '../controller/posts.js';

router.get("/getPosts", getPosts);
router.post("/addPost", addPost);
router.post("/removePost", removePost);
router.post("/updatePost", updatePost);

export default router;