import express from 'express';

const router = express.Router();

import {addComment, getComments} from '../controller/comments.js';

router.get("/getComments", getComments);
router.post("/addComment", addComment);
export default router;