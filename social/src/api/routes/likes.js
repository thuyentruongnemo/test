import express from 'express';

const router = express.Router();

import { getLikes, addLike, removeLike } from '../controller/likes.js';

router.get("/getLikes", getLikes);
router.post("/addLike", addLike);
router.post("/removeLike", removeLike);
export default router;