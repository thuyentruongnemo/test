export interface PostType {
    id: number,
    caption: string,
    img: any,
    userId: number,
    createdAt: Date,
    name: string,
    profilePicture: string
}

export interface Comment {
    id: number,
    comment: string,
    createdAt: Date,
    userId: number,
    postId: number,
    name: string,
    profilePicture: string
}

export interface Profile {
    avatar: string,
    name: string,
    email: string,
    password: string,
    username: string,
    coverPicture: string,
    city: string,
}