import {Avatar, Divider} from "@mui/material";
import BookmarkIcon from "@mui/icons-material/Bookmark";
import MoreVertIcon from "@mui/icons-material/MoreVert";
// import ThumbUpIcon from "@mui/icons-material/ThumbUp";
import CommentIcon from "@mui/icons-material/Comment";
// import ShareIcon from "@mui/icons-material/Share";
import CommentSection from "../comment-section";
import {PostType, Comment} from "../../types/type";
import moment from "moment";
import {useQuery} from "@tanstack/react-query";
import axios from "axios";
import React from "react";

export interface PostInModalProps {
    post: PostType;
    totalLikes: number;
}

const PostInModal = ( props: PostInModalProps ) => {

    const [comments, setComments] = React.useState<Comment[]>([])

    const { isLoading } = useQuery( {
        queryKey: ['comments'],
        queryFn: async () => {
            const response = await axios.get('http://localhost:8800/api/comments/getComments',
                {
                    params:
                        {
                            postId: props.post.id
                        }
                });
            setComments(response.data)
        },
    });

    return (
        <div className="w-full h-fit bg-[#242526] p-4 border-[1px] rounded-[8px] border-[#26292e] pt-2">
            <div className="flex justify-between items-center">
                <div className="flex items-center gap-4">
                    <Avatar src={"/upload/"+props.post.profilePicture} sizes="medium"/>
                    <div>
                        <p className="text-white">{props.post?.name}</p>
                        <p className="text-[#76787b]">{moment(props.post?.createdAt).fromNow()}</p>
                    </div>
                </div>
                <div className="flex items-center space-x-2">
                    <BookmarkIcon className="cursor-pointer rounded-full" sx={{color: "white"}}/>
                    <MoreVertIcon className="cursor-pointer rounded-full hover:bg-[#36393f]" sx={{color: "#76787b"}}/>
                </div>
            </div>
            <div className="mt-1">
                <p className="text-white">{props.post?.caption}</p>
            </div>
            <div className="mt-1 flex justify-center">
                <img src={"../../../public/upload/" + props.post?.img} alt=""/>
            </div>
            <div className="flex justify-around items-center h-12">
                {/*<div className="flex items-center space-x-2 cursor-pointer">*/}
                {/*    <ThumbUpIcon sx={{color: "#76787b"}}/>*/}
                {/*    <p className="text-[#76787b]">Like post</p>*/}
                {/*    <div className="text-white rounded-full bg-[#36393f] px-2">{props.totalLikes}</div>*/}
                {/*</div>*/}
                <div className="flex items-center space-x-2 cursor-pointer">
                    <CommentIcon sx={{color: "#76787b"}}/>
                    <p className="text-[#76787b]">Comment</p>
                    {/*<div className="text-white rounded-full bg-[#36393f] px-2">45</div>*/}
                </div>
                {/*<div className="flex items-center space-x-2 cursor-pointer">*/}
                {/*    <ShareIcon sx={{color: "#76787b"}}/>*/}
                {/*    <p className="text-[#76787b]">Share</p>*/}
                {/*    /!*<div className="text-white rounded-full bg-[#36393f] px-2">45</div>*!/*/}
                {/*</div>*/}
            </div>

            <Divider sx={{borderColor: "#26292e", borderWidth: "1px"}}/>

            {isLoading ? <div>Loading...</div> : comments.map((comment: Comment) => <CommentSection comment={comment}/>)}
        </div>
    )
}

export default PostInModal