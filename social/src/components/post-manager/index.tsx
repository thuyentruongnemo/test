import React from "react";
import {Avatar, Divider, OutlinedInput} from "@mui/material";
import BookmarkIcon from "@mui/icons-material/Bookmark";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import ThumbUpIcon from "@mui/icons-material/ThumbUp";
import CommentIcon from "@mui/icons-material/Comment";
import ShareIcon from "@mui/icons-material/Share";
import Camera from "../../assets/camera-svgrepo-com.svg";
import Image from "../../assets/image-01-svgrepo-com.svg";
import Attach from "../../assets/attach-svgrepo-com.svg";
import Emoji from "../../assets/emoji-smile-svgrepo-com.svg";
import FullPostViewModal from "../full-post-view-modal";
import UpPostModal from "../up-post-modal";
import { PostType } from "../../types/type";
import moment from "moment";
import { Popover } from "antd";
import {useMutation, useQuery, useQueryClient} from "@tanstack/react-query";
import axios from "axios";
import { AuthContext } from "../../context/authContext.tsx";
import { useNavigate, useLocation } from "react-router-dom";

interface PropsType {
    key: number,
    post: PostType
}

interface MoreOverProps {
    handleDelete: () => void;
    handleUpdate: () => void;
}

const MoreOver: React.FC<MoreOverProps> = (props) => {

    const { handleDelete, handleUpdate } = props;

    return (
        <div className="space-y-2 items-center">
            <div onClick={handleUpdate} className="mb-2 rounded-lg p-2 hover:cursor-pointer hover:bg-neutral-300 text-center">Update</div>
            <Divider/>
            <div onClick={handleDelete} className="rounded-lg p-2 hover:cursor-pointer hover:bg-neutral-300 text-center">Delete</div>
        </div>
    )
}

const Post = (props: PropsType) => {

    const [liked, setLiked] = React.useState<boolean>(false)

    const [totalLikes, setTotalLikes] = React.useState<number>(0)

    const modalRef = React.useRef<any>(null);

    const upPostRef = React.useRef<any>(null);

    const queryClient = useQueryClient()

    const navigate = useNavigate()

    const location = useLocation()

    const userId = parseInt(location.pathname.split("/")[2]);

    const { currentUser } = React.useContext(AuthContext);

    useQuery( {
        queryKey: ['likes'],
        queryFn: async () => {
            const response = await axios.get('http://localhost:8800/api/likes/getLikes',
                {
                    params: {
                        postId: props.post.id
                    }
                }
            );
            setTotalLikes(response.data)
        },
    });

    const mutation = useMutation({
        mutationFn: async () => {
            if(liked) {
                try {
                    await axios.post("http://localhost:8800/api/likes/addLike", {
                        userId: currentUser.id,
                        postId: props.post.id
                    })
                } catch (err) {
                    console.log(err)
                }
            } else {
                try {
                    await axios.post("http://localhost:8800/api/likes/removeLike", {
                        userId: currentUser.id,
                        postId: props.post.id
                    })
                } catch (err) {
                    console.log(err)
                }
            }
        },
        onSuccess: () => {
            queryClient.invalidateQueries( { queryKey: ['likes'] } ).then()
        },
    })

    const handleDeletePost = async () => {
        mutationDeletePost.mutate()
    }

    const handleUpdatePost = async () => {
        upPostRef.current?.onEdit(props.post)
    }

    const mutationDeletePost = useMutation({
        mutationFn: async () => {
            try {
                await axios.post("http://localhost:8800/api/posts/removePost", {
                    id: props.post.id
                })
            } catch (err) {
                console.log(err)
            }
        },
        onSuccess: () => {
            queryClient.invalidateQueries( { queryKey: ['posts'] } ).then()
        },
    })

    const handleLikePost = async () => {
        setLiked(!liked)
        mutation.mutate()
    }

    const openModal = () => {
        modalRef.current?.open();
    }

    return (
        <div key={props.key} className="w-full h-fit bg-[#201c24] p-4 border-[1px] rounded-[8px] border-[#26292e] pt-2 mb-8">
            <FullPostViewModal ref={modalRef} post={props.post} totalLikes={totalLikes}/>
            <UpPostModal ref={upPostRef}/>
            <div className="flex justify-between items-center">
                <div onClick={() => navigate('/profile/' + props.post.userId)} className="flex items-center gap-4">
                    <Avatar src={"/upload/" + props.post.profilePicture} sizes="medium"/>
                    <div>
                        <p className="text-white">{props.post.name}</p>
                        <p className="text-[#76787b]">{moment(props.post.createdAt).fromNow()}</p>
                    </div>
                </div>
                <div className="flex items-center space-x-2">
                    <BookmarkIcon className="cursor-pointer rounded-full" sx={{color: "white"}}/>
                    <Popover
                        placement="bottomLeft"
                        content={
                            userId === currentUser.id &&
                            <MoreOver handleUpdate={handleUpdatePost} handleDelete={handleDeletePost}/>
                        }
                        arrow={false}
                        trigger="click"
                    >
                        <MoreVertIcon className="cursor-pointer rounded-full hover:bg-[#36393f]" sx={{color: "#76787b"}}/>
                    </Popover>
                </div>
            </div>
            <div className="mt-1">
                <p className="text-white">{props.post.caption}</p>
            </div>
            <div className="mt-1 flex justify-center">
                <img src={"../../../public/upload/" + props.post.img} alt=""/>
            </div>
            <div className="flex justify-around items-center h-12">
                <div onClick={handleLikePost} className="flex items-center space-x-2 cursor-pointer">
                    <ThumbUpIcon sx={{ color: liked ? "#4e9afa" : "#76787b" }}/>
                    <p className={liked ? "text-[#4e9afa]" : "text-[#76787b]"}>Like post</p>
                    <div className="text-white rounded-full bg-[#36393f] px-2">{totalLikes}</div>
                </div>
                <div onClick={openModal} className="flex items-center space-x-2 cursor-pointer">
                    <CommentIcon sx={{ color: "#76787b" }}/>
                    <p className="text-[#76787b]">Comment</p>
                    {/*<div className="text-white rounded-full bg-[#36393f] px-2">45</div>*/}
                </div>
                <div className="flex items-center space-x-2 cursor-pointer">
                    <ShareIcon sx={{ color: "#76787b" }}/>
                    <p className="text-[#76787b]">Share</p>
                    {/*<div className="text-white rounded-full bg-[#36393f] px-2">45</div>*/}
                </div>
            </div>
            <Divider sx={{borderColor: "#26292e", borderWidth: "1px"}}/>
            <div className="flex justify-around items-center mt-3">
                <Avatar src={"/upload/"+currentUser.profilePicture} sx={{width: "30px", height: "30px"}}/>
                <OutlinedInput
                    onClick={openModal}
                    sx={{
                        color: "#76787b",
                        height: "35px",
                        width: "90%",
                        borderRadius: "9999px",
                        backgroundColor: "#27292f"
                    }}
                    color="secondary"
                    placeholder="What is on your mind"
                    endAdornment={
                        <div className="flex space-x-2 w-32">
                            <img className="h-5 w-5 my-2 cursor-pointer rounded-full hover:bg-[#36393f]" src={Camera}
                                 alt=""/>
                            <img className="h-5 w-5 my-2 cursor-pointer rounded-full hover:bg-[#36393f]" src={Image}
                                 alt=""/>
                            <img className="h-5 w-5 my-2 cursor-pointer rounded-full hover:bg-[#36393f]" src={Attach}
                                 alt=""/>
                            <img className="h-5 w-5 my-2 cursor-pointer rounded-full hover:bg-[#36393f]" src={Emoji}
                                 alt=""/>
                        </div>
                    }
                />
            </div>
        </div>
    )
}

export default Post