import React from "react";
import { Modal } from "antd";
import PostInModal from "../post-manager/post-in-modal.tsx";
import {Avatar, OutlinedInput} from "@mui/material";
import Camera from "../../assets/camera-svgrepo-com.svg";
import Image from "../../assets/image-01-svgrepo-com.svg";
import Attach from "../../assets/attach-svgrepo-com.svg";
import Emoji from "../../assets/emoji-smile-svgrepo-com.svg";
import {PostType} from "../../types/type";
import {useMutation, useQueryClient} from "@tanstack/react-query";
import axios from "axios";
import {AuthContext} from "../../context/authContext.tsx";

export interface FullPostViewModalProps {
    post: PostType;
    totalLikes: number;
}

const FullPostViewModal = (props: FullPostViewModalProps, ref: any ) => {
    
    const [isVisible, setVisible] = React.useState<boolean>(false);

    const [newComment, setNewComment] = React.useState<string>("");

    const queryClient = useQueryClient()

    const { currentUser } = React.useContext(AuthContext);

    React.useImperativeHandle(
        ref,
        () => ({
            open: () => setVisible(true),
            close: () => setVisible(false),
        }),
        [setVisible]
    )

    const mutation = useMutation({
        mutationFn: async (comment: string) => {
            try {
                await axios.post("http://localhost:8800/api/comments/addComment", {
                    postId: props.post.id,
                    userId: currentUser.id,
                    comment: comment
                })
            } catch (err) {
                console.log(err)
            }
            setNewComment("")
        },
        onSuccess: () => {
            queryClient.invalidateQueries( { queryKey: ['comments'] } ).then()
        },
    })

    const handlePublicComment = (event: any) => {
        if(event.key == "Enter") {
            mutation.mutate(event.target.value)
        }
    }

    const handleChange = (event: any) => {
        setNewComment(event.target.value)
    }
    
    return (
        <Modal
            title={`${props.post.name}'s post`}
            open={isVisible}
            onCancel={() => setVisible(false)}
            width={700}
            centered={true}
            footer={[
                <div className="flex justify-around items-center mt-3 mr-3 py-2">
                    <Avatar src={"/upload/"+currentUser.profilePicture} sx={{width: "30px", height: "30px", marginBottom: "0.5rem",}}/>
                    <OutlinedInput
                        onKeyDown={handlePublicComment}
                        onChange={handleChange}
                        value={newComment}
                        sx={{
                            color: "white",
                            height: "40px",
                            width: "90%",
                            borderRadius: "9999px",
                            backgroundColor: "#3a3b3c",
                            marginBottom: "0.5rem",
                        }}
                        endAdornment={
                            <div className="flex space-x-2 w-32">
                                <img className="h-5 w-5 my-2 cursor-pointer rounded-full hover:bg-[#36393f]"
                                     src={Camera}
                                     alt=""/>
                                <img className="h-5 w-5 my-2 cursor-pointer rounded-full hover:bg-[#36393f]" src={Image}
                                     alt=""/>
                                <img className="h-5 w-5 my-2 cursor-pointer rounded-full hover:bg-[#36393f]"
                                     src={Attach}
                                     alt=""/>
                                <img className="h-5 w-5 my-2 cursor-pointer rounded-full hover:bg-[#36393f]" src={Emoji}
                                     alt=""/>
                            </div>
                        }
                    />
                </div>
            ]}
            destroyOnClose={true}
            className="border-none"
            bodyStyle={{maxHeight: "70vh", overflowY: "auto"}}
        >
            <div className="relative">
                <PostInModal post={props.post} totalLikes={props.totalLikes}/>
            </div>
        </Modal>
    )
}

export default React.memo(React.forwardRef(FullPostViewModal))