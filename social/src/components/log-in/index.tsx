import React from 'react';
import LoginPage from "./log-in.tsx";
import RegisterPage from "./register.tsx";

const SignInPage: React.FC = () => {

    const [loginPage, setLoginPage] = React.useState<boolean>(true);

    const isLoginPage = (value: boolean) => {
        setLoginPage(value);
    }

    return (
        <div className="w-screen h-screen flex justify-around items-center">
            <div className="flex bg-[#f9f5ec] w-4/5 h-4/5 rounded-[15px]">
                <div className="w-1/2 h-full flex items-center">
                    {loginPage ? <LoginPage setLoginPage={isLoginPage}/> : <RegisterPage setLoginPage={isLoginPage}/>}
                </div>
                <div className="w-1/2 h-full flex justify-center items-center">
                    <img className="w-[90%] h-[90%] rounded-[15px]" src="https://lab.cccb.org/wp-content/uploads/fuecocomeme.jpg" alt=""/>
                </div>
            </div>
        </div>
    );
}

export default React.memo(SignInPage)