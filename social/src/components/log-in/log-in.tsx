import React from 'react';
import {FormControl, IconButton, InputAdornment, InputLabel, Input, TextField} from "@mui/material";
import {Visibility, VisibilityOff} from "@mui/icons-material";
import {Button, Checkbox} from "antd";
import { AuthContext } from "../../context/authContext";
import { useNavigate } from "react-router-dom";

const LoginPage: React.FC<{ setLoginPage: (value: boolean) => void }> = (props: any) => {

    const { setLoginPage } = props;

    const { login } = React.useContext(AuthContext);

    const navigate = useNavigate();

    const [showPassword, setShowPassword] = React.useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    const [username, setUsername] = React.useState<string>('');
    const [password, setPassword] = React.useState<string>('');

    const [err, setErr] = React.useState<any>(null);

    const handleUsername = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUsername(event.target.value);
    };

    const handlePassword = (event: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(event.target.value);
    };

    const handleLoginAccount = async () => {
      try {
        await login({ username, password }).then(() => {
            navigate("/home")
        });
      } catch (error) {
          setErr(error)
        }
    };

    return (
        <div className="w-full justify-center items-center">
            <div className="w-full flex-col justify-center mb-4">
                <p className="text-4xl font-bold text-blue-500 text-center mb-4">KALOKA</p>
                <p className="text-center font-bold text-2xl mb-1">Welcome back</p>
                <p className="text-center">Access your personal account by logging in</p>
            </div>
            <div className="grid items-center justify-center mb-4">
                <TextField
                    sx={{ m: 1, width: '35ch' }}
                    label="Username"
                    id="standard-size-small"
                    variant="standard"
                    onChange={handleUsername}
                />
                <FormControl sx={{ m: 1, width: '35ch' }} variant="standard">
                    <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
                    <Input
                        onChange={handlePassword}
                        id="standard-adornment-password"
                        type={showPassword ? 'text' : 'password'}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                >
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>
                {err && err.response && <p className="text-red-500 text-center">{err.response.data.message}</p>}
            </div>
            <div className="w-full flex justify-center mb-4">
                <div className="w-[35ch] flex justify-between">
                    <Checkbox>Remember me</Checkbox>
                    <span className="text-[13px] hover:underline hover:cursor-pointer">Forgot Password?</span>
                </div>
            </div>
            <div className="w-full flex-col items-center justify-center">
                <div className="flex justify-center mb-4">
                    <Button onClick={handleLoginAccount} className="w-[40ch] border-blue-500">Log in</Button>
                </div>
                <div className="flex justify-center text-[13px] gap-2">
                    <p>Don't have an account?</p>
                    <span onClick={() => setLoginPage(false)} className="hover:underline hover:cursor-pointer">Sign up</span>
                </div>
            </div>
        </div>
    );
};

export default LoginPage;