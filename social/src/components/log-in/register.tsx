import React from 'react';
import {FormControl, IconButton, InputAdornment, InputLabel, Input, TextField} from "@mui/material";
import {Visibility, VisibilityOff} from "@mui/icons-material";
import {Button} from "antd";
import axios from "axios";

const RegisterPage: React.FC<{ setLoginPage: (value: boolean) => void }> = (props: any) => {

    const { setLoginPage } = props;

    const [showPassword, setShowPassword] = React.useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    const [input, setInput] = React.useState({
        name: '',
        username: '',
        email: '',
        password: ''
    });

    const [err, setErr] = React.useState<any>(null);

    const handleChangeName = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInput((oldState) => {
            return { ...oldState, name: event.target.value }
        })
    }

    const handleChangeUsername = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInput((oldState) => {
            return { ...oldState, username: event.target.value }
        })
    }

    const handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInput((oldState) => {
            return { ...oldState, email: event.target.value }
        })
    }

    const handleChangePassword = (event: React.ChangeEvent<HTMLInputElement>) => {
        setInput((oldState) => {
            return { ...oldState, password: event.target.value }
        })
    }

    const handleRegisterAccount = async (event: any) => {
        event.preventDefault();
        try {
            await axios.post('http://localhost:8800/api/auth/register', input).then(() => {
                setLoginPage(true);
            })
        } catch (error) {
            setErr(error);
        }
    };

    console.log("error", err);

    return (
        <div className="w-full justify-center items-center">
            <div className="w-full flex-col justify-center mb-4">
                <p className="text-4xl font-bold text-blue-500 text-center mb-4">KALOKA</p>
                <p className="text-center font-bold text-2xl mb-1">Register</p>
                {/*<p className="text-center">Access your personal account by logging in</p>*/}
            </div>
            <div className="grid items-center justify-center mb-4">
                <TextField
                    onChange={handleChangeName}
                    sx={{ m: 1, width: '35ch' }}
                    label="Name"
                    id="standard-size-small"
                    variant="standard"
                />
                <TextField
                    onChange={handleChangeUsername}
                    sx={{ m: 1, width: '35ch' }}
                    label="Username"
                    id="standard-size-small"
                    variant="standard"
                />
                <TextField
                    onChange={handleChangeEmail}
                    sx={{ m: 1, width: '35ch' }}
                    label="Email"
                    id="standard-size-small"
                    variant="standard"
                />
                <FormControl sx={{ m: 1, width: '35ch' }} variant="standard">
                    <InputLabel htmlFor="standard-adornment-password">Password</InputLabel>
                    <Input
                        onChange={handleChangePassword}
                        id="standard-adornment-password"
                        type={showPassword ? 'text' : 'password'}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                >
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                    />
                </FormControl>
                {err && err.response && <p className="text-red-500 text-center">{err.response.data.message}</p>}
            </div>
            <div className="w-full flex-col items-center justify-center">
                <div className="flex justify-center mb-4">
                    <Button onClick={handleRegisterAccount} className="w-[40ch] border-blue-500">Register</Button>
                </div>
                <div className="flex justify-center text-[13px] gap-2">
                    <p>Already have an account?</p>
                    <span onClick={() => setLoginPage(true)} className="hover:underline hover:cursor-pointer">Log in</span>
                </div>
            </div>
        </div>
    );
};

export default RegisterPage;