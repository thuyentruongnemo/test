import React from "react";
import UpPostModal from "../up-post-modal";
import OutlinedInput from "@mui/material/OutlinedInput";
import Camera from "../../assets/camera-svgrepo-com.svg";
import Image from "../../assets/image-01-svgrepo-com.svg";
import Attach from "../../assets/attach-svgrepo-com.svg";
import Location from "../../assets/location-pin-svgrepo-com.svg";
import Emoji from "../../assets/emoji-smile-svgrepo-com.svg";
import Button from "@mui/material/Button";
import NewFeeds from "../newfeed";
import {AuthContext} from "../../context/authContext.tsx";
import {Avatar} from "@mui/material";
import {useNavigate} from "react-router-dom";

const MainSection = () => {

    const { currentUser } = React.useContext(AuthContext);

    const modalRef = React.useRef<any>(null)

    const navigate = useNavigate()

    const openModal = () => {
        modalRef.current?.open();
    }

    return (
        <div className="w-2/3 overflow-y-scroll">
            <div
                className="flex flex-col justify-center gap-2 bg-[#201c24] mx-16 my-4 h-[17%] border-[1px] rounded-[8px] border-[#26292e]">
                <UpPostModal ref={modalRef}/>
                <div className="flex justify-around mt-2">
                    <div className="h-10 w-10 rounded-full bg-white ml-4">
                        <Avatar
                            onClick={() => navigate("/profile/"+currentUser.id)}
                            sx={{
                                marginLeft: 0
                            }}
                            src={"../../../public/upload/"+currentUser.profilePicture}
                        />
                    </div>
                    <OutlinedInput onClick={openModal} sx={{
                        color: "#76787b",
                        height: "40px",
                        width: "85%",
                        borderRadius: "9999px",
                        backgroundColor: "#27292f"
                    }} color="secondary" placeholder="What is on your mind"/>
                </div>

                <div className="flex justify-between">
                    <div className="flex ml-4 items-center">
                        <img onClick={openModal}
                             className="h-8 w-8 p-1 mx-4 my-2 cursor-pointer rounded-full hover:bg-[#36393f]"
                             src={Camera} alt=""/>
                        <img onClick={openModal}
                             className="h-8 w-8 p-1 mx-4 my-2 cursor-pointer rounded-full hover:bg-[#36393f]"
                             src={Image} alt=""/>
                        <img onClick={openModal}
                             className="h-8 w-8 p-1 mx-4 my-2 cursor-pointer rounded-full hover:bg-[#36393f]"
                             src={Attach} alt=""/>
                        <img onClick={openModal}
                             className="h-8 w-8 p-1 mx-4 my-2 cursor-pointer rounded-full hover:bg-[#36393f]"
                             src={Location} alt=""/>
                        <img onClick={openModal}
                             className="h-8 w-8 p-1 mx-4 my-2 cursor-pointer rounded-full hover:bg-[#36393f]"
                             src={Emoji} alt=""/>
                    </div>
                    <div className="flex justify-center w-24 items-center">
                        <Button
                            sx={{
                                color: "#76787b",
                                borderColor: "#76787b",
                                height: "30px",
                                marginTop: "2px"
                            }}
                            variant="outlined"
                        >
                            Post
                        </Button>
                    </div>
                </div>
            </div>
            <div className="mx-16 my-4 h-[78%]">
                <NewFeeds/>
                <div className="h-2"/>
            </div>
        </div>
    );
}

export default React.memo(MainSection)