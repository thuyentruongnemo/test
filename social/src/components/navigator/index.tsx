import HomeIcon from "@mui/icons-material/Home";
import { Menu, Popover } from "antd";
import {Avatar, Divider} from "@mui/material";
import "./index.scss";
import ChatIcon from '@mui/icons-material/Chat';
import NotificationsIcon from '@mui/icons-material/Notifications';
import {AuthContext} from "../../context/authContext.tsx";
import React from "react";
import {useNavigate} from "react-router-dom";

const Navigator = () => {

    const { currentUser } = React.useContext(AuthContext)

    const items = [
        {
            key: "Explore",
            label: "Explore",
        },
        {
            key: "Community Feed",
            label: "Community Feed",
        },
        {
            key: "Mutual Friends",
            label: "Mutual Friends",
        },
    ]

    const navigate = useNavigate()

    return (
        <div className="w-full h-full bg-[#201c24] flex">
            <div className="flex w-3/5 h-full items-center pl-6 gap-8">
                <div className="flex items-center gap-3">
                    <HomeIcon sx={{ color: "white" }} fontSize="large"/>
                    <span className="text-white text-2xl font-[650px]">Home</span>
                </div>
                <div className="bg-[#27292f] p-1 rounded-lg">
                    <Menu
                        defaultSelectedKeys={["Explore"]}
                        className="navigator"
                        mode="horizontal"
                        items={items}
                    />
                </div>
            </div>
            <div className="flex justify-end items-center w-2/5 gap-4 pr-4">
                <Popover placement="bottom" content={<div className="w-fit h-4 mt-0">Messages</div>} arrow={false}>
                    <Popover placement="bottom" content={<div className="w-32 h-32"></div>} arrow={false} trigger="click">
                        <div className="cursor-pointer bg-[#36393f] p-2 rounded-full flex items-center">
                            <ChatIcon sx={{ color: "white" }} fontSize="medium"/>
                        </div>
                    </Popover>
                </Popover>
                <Popover placement="bottom" content={<div className="w-fit h-4 mt-0">Notifications</div>} arrow={false}>
                    <Popover placement="bottom" content={<div className="w-32 h-32"></div>} arrow={false} trigger="click">
                        <div className="cursor-pointer bg-[#36393f] p-2 rounded-full flex items-center">
                            <NotificationsIcon sx={{ color: "white" }} fontSize="medium"/>
                        </div>
                    </Popover>
                </Popover>   
                <div className="flex items-center">
                    <span className="text-white">{currentUser.name}</span>
                    <Popover placement="bottom"
                             content={
                                 <div>
                                     <div onClick={() => {
                                         navigate('/profile/' + currentUser.id)
                                     }}
                                          className="text-center rounded-lg px-2 py-1 hover:cursor-pointer hover:bg-[#b1b0b3]"
                                     >
                                         Profile
                                     </div>
                                     <Divider sx={{ marginY: "2px" }}/>
                                     <div onClick={() => {
                                         navigate('/')
                                         localStorage.removeItem("user")
                                     }}
                                          className="text-center rounded-lg px-2 py-1 hover:cursor-pointer hover:bg-[#b1b0b3]"
                                     >
                                         Log out
                                     </div>
                                 </div>
                             }
                             arrow={false}
                             trigger="click"
                    >
                        <div className="cursor-pointer flex items-center">
                            <Avatar sx={{ width: 32, height: 32 }} src={"/upload/"+currentUser.profilePicture}/>
                        </div>
                    </Popover>
                </div>
            </div>
        </div>
    );
};

export default Navigator;