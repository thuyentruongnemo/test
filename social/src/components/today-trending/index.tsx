import { Collapse,Button } from 'antd';
import React from 'react';
import "./index.scss";

interface Item {
    title: string,
    description: string,
    key: string,
}

const TodayTrendingItem: React.FC<{items: Item[]}> = ({ items }) => {

    return (
        <div>
            {items.map((item: Item) => (
                    <div key={item.key} className='flex flex-col border-1 border-[#26292e] mb-2'>
                        <span className='text-white'>{item.title}</span>
                        <span className='text-[#76787b]'>{item.description}</span>
                    </div>
                )
            )}
        </div>
    )
}

const TodayTrending = () => {

    const itemsList: Item[] = [
        {
            title: "Figma maintenance",
            description: "125 posts today",
            key: "1",
        },
        {
            title: "Figma maintenance",
            description: "125 posts today",
            key: "2",
        },
    ]

    const items = [
        {
            key: '1',
            label: 'Today Trending',
            children: <TodayTrendingItem items={itemsList}/>,
            showArrow: false,
        },
    ]

    return (
        <div className="w-full h-full">
            <div className="mr-12 ml-6 my-4">
                <Collapse items={items} defaultActiveKey={["1"]} />
            </div>
            <div className='ml-6 mr-12 rounded-lg bg-[#201c24] border-[1px] border-[#26292e] space-y-4'>
                <img className='w-full rounded-t-lg' src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTN4a0uswlJQ8aP9Z822DbTBrtnR5CqbGaPFlo6GGL_ThbomdodYJhm_i3RfZEMHXi_Yoc&usqp=CAU" alt="" />
                <div className='space-y-2 pl-4 pb-2 pr-4'>
                    <span className='text-white'>Figma Designer</span>
                    <div className='space-x-4'>
                        <span className='text-[#76787b]'>1425 members</span>
                        <span className='text-[#76787b]'>125 posts/day</span>
                    </div>
                    <Button className='w-full h-10 text-blue-300 border-2 border-[#26292e]'>Join Community</Button>
                </div>
            </div>
        </div>
    );
};

export default TodayTrending;