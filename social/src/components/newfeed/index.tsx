import Post from "../post-manager";
import { PostType } from "../../types/type";
import axios from "axios";
import React from "react";
import { useQuery } from "@tanstack/react-query";

export interface NewFeedsProps {
    userId?: number;
}

const NewFeeds: React.FC<NewFeedsProps> = (props) => {

    const [posts, setPosts] = React.useState<PostType[]>([])

    const { isLoading } = useQuery( {
        queryKey: ['posts'],
        queryFn: async () => {
            if(props.userId) {
                const response = await axios.get('http://localhost:8800/api/posts/getPosts',
                    {
                        params: {
                            userId: props.userId
                        }
                    });
                setPosts(response.data.reverse())
            } else {
                const response = await axios.get('http://localhost:8800/api/posts/getPosts');
                setPosts(response.data.reverse())
            }
        },
    });

    return (
        <div className="w-full">
            {
                isLoading ? <div>Loading...</div> :
                posts.map((post: PostType, index) => <Post key={index} post={post}/>)
            }
        </div>
    );
};

export default NewFeeds;