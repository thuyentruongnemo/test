import React, { forwardRef } from "react";
import {Modal, Input, Button} from "antd";
import {useMutation, useQueryClient} from "@tanstack/react-query";
import axios from "axios";
import { getUser } from "../../redux/reducers/get-user.ts";
import { useDispatch } from "react-redux";
import {Avatar} from "@mui/material";

const EditProfileModal = (props: any, ref: any) => {

    const [visible, setVisible] = React.useState(false);

    const [fileCover, setFileCover] = React.useState<null | Blob>();

    const [fileProfile, setFileProfile] = React.useState<null | Blob>();

    const queryClient = useQueryClient()

    const dispatch = useDispatch()

    const [username, setUsername] = React.useState<string>("")

    const [name, setName] = React.useState<string>("")

    const [password, setPassword] = React.useState<string>("")

    const [email, setEmail] = React.useState<string>("")

    const [city, setCity] = React.useState<string>("")

    React.useEffect(() => {
        if (props.user) {

            setUsername(props.user.username)
            setName(props.user.name)
            setPassword(props.user.password)
            setEmail(props.user.email)
            setCity(props.user.city)
        }
    }, [props.user])

    React.useImperativeHandle(ref, () => ({
        open: () => {
            setVisible(true);
        },
        close: () => {
            setVisible(false);
        }
    }), [setVisible]);

    const upload = async (file: Blob) => {
        try {
            const formData = new FormData()
            formData.append("file", file as Blob)
            const res = await axios.post("http://localhost:8800/api/upload", formData)
            return res.data
        } catch (err) {
            console.log(err)
        }
    }

    const getFileCover = (event: any) => {
        if(event.target.files && event.target.files.length > 0) {
            setFileCover(event.target.files[0])
        } else {
            setFileCover(null)
        }
    }

    const getFileProfile = (event: any) => {
        if(event.target.files && event.target.files.length > 0) {
            setFileProfile(event.target.files[0])
        } else {
            setFileProfile(null)
        }
    }

    const mutation = useMutation({
        mutationFn: async ({profilePic, coverPic}: {profilePic: string, coverPic: string}) => {
            try {
                await axios.post("http://localhost:8800/api/users/updateUser", {
                    coverPicture: coverPic,
                    profilePicture: profilePic,
                    username: username,
                    name: name,
                    password: password,
                    email: email,
                    city: city,
                    id: props.user.id
                })
                dispatch(getUser({id: props.user.id}))
            } catch (err) {
                console.log(err)
            }
            setFileProfile(null)
            setFileCover(null)
            setCity("")
            setName("")
            setPassword("")
            setEmail("")
            setUsername("")
            setVisible(false)
        },
        onSuccess: () => {
            queryClient.invalidateQueries( { queryKey: ['user'] } ).then()
        },
    })

    const handleUserName = (event: any) => {
        setUsername(event.target.value)
    }

    const handleName = (event: any) => {
        setName(event.target.value)
    }

    const handleEmail = (event: any) => {
        setEmail(event.target.value)
    }

    const handlePassword = (event: any) => {
        setPassword(event.target.value)
    }

    const handleCity = (event: any) => {
        setCity(event.target.value)
    }

    const handleSave = async () => {
        let coverPic = props.user.coverPicture
        let profilePic = props.user.profilePicture
        if (fileCover) {
            coverPic = await upload(fileCover)
        }
        if (fileProfile) {
            profilePic = await upload(fileProfile)
        }
        mutation.mutate({profilePic, coverPic})
    }

    return (
        <Modal
            onCancel={() => {
                setFileProfile(null)
                setFileCover(null)
                setCity(props.user.city)
                setName(props.user.name)
                setPassword(props.user.password)
                setEmail(props.user.email)
                setUsername(props.user.username)
                setVisible(false)
            }}
            open={visible}
            centered
            title="Edit Profile"
            footer={false}
            width={800}
            destroyOnClose={true}
        >
            <div className="overflow-y-scroll mt-4">
                <div className="flex w-full">
                    <div className="w-1/2 ml-4">
                        <p className="text-white mb-2 text-xl">Edit Cover Picture</p>
                        <div className="flex h-20 space-x-8 items-center">
                            <label htmlFor="cover" className="bg-white h-10 px-4 py-2 rounded-lg">Choose file</label>
                            <input style={{display: "none"}} id="cover" onChange={getFileCover} type="file"/>
                            {
                                !fileCover && !props.user.coverPicture ?
                                    <div className="w-1/3 bg-[#17181c] pt-16"/> :
                                    <img
                                        src={
                                            fileCover ?
                                                URL.createObjectURL(fileCover as Blob) :
                                                "/upload/" + props.user.coverPicture
                                        }
                                        style={{objectFit: "cover", objectPosition: "0% 0%"}}
                                        alt=""
                                        className="w-1/3 h-16 top-0"
                                    />
                            }
                        </div>
                    </div>
                    <div className="w-1/2 ml-4">
                        <p className="text-white mb-2 text-xl">Edit Profile Picture</p>
                        <div className="flex h-20 space-x-8 items-center">
                            <label htmlFor="profile" className="bg-white h-10 px-4 py-2 rounded-lg">Choose file</label>
                            <input style={{display: "none"}} id="profile" onChange={getFileProfile} type="file"/>
                            {!fileProfile && !props.user.profilePicture ?
                                <Avatar sx={{ width: "5rem", height: "5rem" }}/> :
                                <img
                                    src={
                                        fileProfile ?
                                            URL.createObjectURL(fileProfile as Blob) :
                                            "/upload/" + props.user.profilePicture
                                    }
                                    style={{ objectFit: "cover", objectPosition: "0% 0%" }}
                                    alt=""
                                    className="h-20 w-20 top-0 rounded-full"
                                />
                            }
                        </div>
                    </div>
                </div>
                <div className="flex w-full mt-4">
                    <div className="w-1/2 flex items-center space-x-4 justify-between mr-4">
                        <label className="text-white text-xl ml-4">Username</label>
                        <Input rootClassName="text-black" style={{ width: "16rem" }} value={username} onChange={handleUserName}/>
                    </div>
                    <div className="w-1/2 flex items-center space-x-4 justify-between mr-4">
                        <label className="text-white text-xl ml-4">Email</label>
                        <Input rootClassName="text-black" style={{ width: "16rem" }} value={email} onChange={handleEmail}/>
                    </div>
                </div>
                <div className="flex w-full mt-4">
                    <div className="w-1/2 flex items-center space-x-4 justify-between mr-4">
                        <label className="text-white text-xl ml-4">Password</label>
                        <Input rootClassName="text-black" style={{ width: "16rem" }} value={password} onChange={handlePassword}/>
                    </div>
                    <div className="w-1/2 flex items-center space-x-4 justify-between mr-4">
                        <label className="text-white text-xl ml-4">Name</label>
                        <Input rootClassName="text-black" style={{ width: "16rem" }} value={name} onChange={handleName}/>
                    </div>
                </div>
                <div className="w-1/2 flex items-center space-x-4 justify-between mt-4">
                    <label className="text-white text-xl ml-4">City</label>
                    <Input rootClassName="text-black" style={{ width: "16rem", marginRight: "1rem" }} value={city} onChange={handleCity}/>
                </div>
            </div>
            <div className="flex justify-center mt-4 h-16 items-center">
                <Button className="h-10 w-24 bg-[#0404ea] border-none text-white" onClick={handleSave}>Save</Button>
            </div>
        </Modal>
    )
}

export default React.memo(forwardRef(EditProfileModal));