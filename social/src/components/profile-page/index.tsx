import React from "react";
import {Button} from "antd";
import "./index.scss";
import NewFeeds from "../newfeed";
import EditIcon from '@mui/icons-material/Edit';
import {useLocation} from "react-router-dom";
import EditProfileModal from "./edit-profile-modal.tsx";
import {useQuery} from "@tanstack/react-query";
import axios from "axios";
import { Avatar } from "@mui/material";
import { AuthContext } from "../../context/authContext.tsx";

const ProfilePage = () => {

    const userId = parseInt(useLocation().pathname.split("/")[2]);

    const modalRef = React.useRef<any>(null);

    const [user, setUser] = React.useState<any>({});

    const { currentUser } = React.useContext(AuthContext);

    const handleOpenModal = () => {
        modalRef.current?.open();
    }

    useQuery( {
        queryKey: ['user'],
        queryFn: async () => {
            const response = await axios.get('http://localhost:8800/api/users/find/' + userId)
            setUser(response.data)
        },
    });

    return (
        <div className="w-2/3 overflow-y-scroll ml-6">
            <EditProfileModal ref={modalRef} user={{...user}}/>
            <div className="w-full border-2 border-t-0 bg-[#201c24] rounded-lg border-[#26292e] mb-6">
                <div className="w-full">
                    {!user.coverPicture
                        ?
                        <div className="w-full bg-[#17181c]" style={{ paddingTop: "18rem" }}/>
                        :
                        <img
                            style={{ objectFit: "cover", objectPosition: "0% 0%" }}
                            className="w-full h-64 rounded-b-lg"
                            src={"../../../public/upload/"+user.coverPicture}
                            alt=""
                        />
                    }
                </div>
                <div className="flex justify-between">
                    <div className="flex">
                        <Avatar
                            sx={{
                                width: '10rem',
                                height: '10rem',
                                borderRadius: '50%',
                                marginLeft: '2.5rem',
                                transform: 'translateY(-33%)'
                            }}
                            src={"../../../public/upload/"+user.profilePicture}
                        />
                        <div>
                            <div className="mt-3 ml-6 text-white text-4xl font-[600px]">{user.name}</div>
                            <div className="mt-1 ml-6 text-[#b1b0b3] font-[600px]">100 friends</div>
                            <div className="flex ml-6">
                                <img
                                    className="border-2 border-[#201c24] z-40 bg-blue-300 rounded-full w-8 h-8"
                                    style={{
                                        backgroundSize: "cover",
                                        backgroundPosition: "center",
                                    }}
                                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSmrxpqFYJ-KnOH9CLfjwotAqPjqWWpXCCBRw&usqp=CAU"
                                    alt=""
                                />
                                <img
                                    className="border-2 border-[#201c24] z-40 bg-blue-300 rounded-full w-8 h-8 transform -translate-x-2"
                                    style={{
                                        backgroundSize: "cover",
                                        backgroundPosition: "center",
                                    }}
                                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS_MBckJ_FSwCsbMgPI6JkPbLoHTUGrKiYUBw&usqp=CAU"
                                    alt=""
                                />
                                <img
                                    className="border-2 border-[#201c24] z-40 bg-blue-300 rounded-full w-8 h-8 transform -translate-x-4"
                                    style={{
                                        backgroundSize: "cover",
                                        backgroundPosition: "center",
                                    }}
                                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZYU4p56Lb6cmmqUeuzJKuYMEOkx_TI92kNw&usqp=CAU"
                                    alt=""
                                />
                                <img
                                    className="border-2 border-[#201c24] z-40 bg-blue-300 rounded-full w-8 h-8 transform -translate-x-6"
                                    style={{
                                        backgroundSize: "cover",
                                        backgroundPosition: "center",
                                    }}
                                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQV1Th8gJ25Pg0Z0rxXjNxczrdBU_WARz-pWw&usqp=CAU"
                                    alt=""
                                />
                                <img
                                    className="border-2 border-[#201c24] z-40 bg-blue-300 rounded-full w-8 h-8 transform -translate-x-8"
                                    style={{
                                        backgroundSize: "cover",
                                        backgroundPosition: "center",
                                    }}
                                    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRlQJ22clk53OolOouSDYQ_7fOJn42cob0pyw&usqp=CAU"
                                    alt=""
                                />
                            </div>
                        </div>
                    </div>
                    {currentUser.id === userId &&
                        <Button onClick={handleOpenModal}
                            className="text-white text-xl h-12 bg-[#3a3b3c] border-none mt-4 mr-6 items-center"
                        >
                            <EditIcon sx={{marginRight: "5px", marginBottom: "4px"}}/>
                            Update Profile
                        </Button>
                    }
                </div>
            </div>
            <div className="w-full flex justify-center">
                <div className="w-[86%] flex justify-center items-center">
                    <NewFeeds userId={userId}/>
                </div>
            </div>
        </div>
    );
}

export default React.memo(ProfilePage);