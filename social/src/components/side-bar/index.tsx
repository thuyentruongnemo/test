import Divider from '@mui/material/Divider';
import { InputAdornment, OutlinedInput, Avatar } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import HomeIcon from '@mui/icons-material/Home';
import GroupsIcon from '@mui/icons-material/Groups';
import EventAvailableIcon from '@mui/icons-material/EventAvailable';
import LocalGroceryStoreIcon from '@mui/icons-material/LocalGroceryStore';
import { Menu, List } from "antd";
import "./index.scss";
import { useNavigate } from "react-router-dom";

const SideBar = () => {

    const navigate = useNavigate()

    const items = [
        {
            key: "Home",
            icon: <HomeIcon style={{ fontSize: "150%" }}/>,
            label: "Home",
            onClick: () => navigate("/home")
        },
        {
            key: "Community",
            icon: <GroupsIcon style={{ fontSize: "150%" }}/>,
            label: "Community"
        },
        {
            key: "Marketplace",
            icon: <LocalGroceryStoreIcon style={{ fontSize: "150%" }}/>,
            label: "Marketplace"
        },
        {
            key: "Events",
            icon: <EventAvailableIcon style={{ fontSize: "150%" }}/>,
            label: "Events"
        },
    ]

    const data = [
        {
            title: "Indonesia UI Designer",
            description: "734 members",
            avatar: "https://lab.cccb.org/wp-content/uploads/fuecocomeme.jpg",
        },
        {
            title: "Indonesia UX Reseacher",
            description: "734 members",
            avatar: "https://lab.cccb.org/wp-content/uploads/fuecocomeme.jpg",
        },
        {
            title: "Frontend Developer Indonesia",
            description: "734 members",
            avatar: "https://lab.cccb.org/wp-content/uploads/fuecocomeme.jpg",
        },
        {
            title: "Indonesia Website Developer",
            description: "734 members",
            avatar: "https://lab.cccb.org/wp-content/uploads/fuecocomeme.jpg",
        }
    ]

    return (
        <div className="w-full h-screen bg-[#201c24] flex flex-col">
            <div className="flex gap-6 pt-4 pl-4">
                <img className="w-16 h-16 rounded-full" src="https://lab.cccb.org/wp-content/uploads/fuecocomeme.jpg" alt="" />
                <span className="text-blue-400 font-bold leading-[4rem] align-middle text-2xl">KALOKA</span>
            </div>
            <div className="pl-4">
                <OutlinedInput
                    sx={{
                        color: "#76787b",
                        height: "40px",
                        width: "95%",
                        backgroundColor: "#27292f",
                        borderRadius: "8px",
                        borderColor: "#31333c",
                        borderWidth: "2px",
                        marginTop: "20px"
                    }}
                    color="secondary"
                    placeholder="Explore Kaloka"
                    startAdornment={
                        <InputAdornment position="start">
                            <SearchIcon fontSize="medium" style={{ color: '#76787b' }} />
                        </InputAdornment>
                    }
                />
            </div>
            <div className="grow overflow-y-scroll">

                <div className="mt-2">
                    <Menu
                        rootClassName=""
                        defaultSelectedKeys={['Home']}
                        defaultOpenKeys={['sub1']}
                        mode="inline"
                        theme="dark"
                        items={items}
                    />
                </div>

                <Divider variant="middle" sx={{ borderColor: "#76787b", marginY: "8px" }} />

                <div>
                    <div className="text-white pl-4">
                        My Community
                    </div>

                    <List
                        itemLayout='horizontal'
                        dataSource={data}
                        renderItem={(item) => (
                            <List.Item
                                className="flex pl-6 py-2 mt-2 items-center hover:bg-[#36393f] rounded-lg cursor-pointer"
                            >
                                <Avatar src={item.avatar} />
                                <div className="flex flex-col text-white">
                                    <span>{item.title}</span>
                                    <span className="text-[#76787b] text-xs">{item.description}</span>
                                </div>
                            </List.Item>
                        )}
                    />

                <Divider variant="middle" sx={{ borderColor: "#76787b", marginY: "8px" }} />
                <div className="text-white pl-4">
                        Upcoming event
                    </div>
                    <List
                        itemLayout='horizontal'
                        dataSource={data}
                        renderItem={(item) => (
                            <List.Item
                                className="flex pl-6 py-2 mt-2 items-center hover:bg-[#36393f] rounded-lg cursor-pointer"
                            >
                                <Avatar src={item.avatar} />
                                <div className="flex flex-col text-white">
                                    <span>{item.title}</span>
                                    <span className="text-[#76787b] text-xs">{item.description}</span>
                                </div>
                            </List.Item>
                        )}
                    />
                </div>
            </div>

        </div>
    );
};

export default SideBar;