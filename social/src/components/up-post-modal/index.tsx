import React from "react";
import {Button, ColorPicker, Input, Modal} from "antd";
import "./index.scss";
import CloseIcon from '@mui/icons-material/Close';
import {Avatar} from "@mui/material";
import Emoji from "../../assets/emoji-smile-svgrepo-com.svg";
import Camera from "../../assets/camera-svgrepo-com.svg";
import Attach from "../../assets/attach-svgrepo-com.svg";
import Location from "../../assets/location-pin-svgrepo-com.svg";
import Image from "../../assets/image-01-svgrepo-com.svg";
import {AuthContext} from "../../context/authContext.tsx";
import axios from "axios";
import {useMutation, useQueryClient} from "@tanstack/react-query";

import type {Color} from "antd/es/color-picker";
import {PostType} from "../../types/type";

const { TextArea } = Input;

const UpPostModal = (_: any, ref: any) => {

    const { currentUser } = React.useContext(AuthContext);

    const queryClient = useQueryClient()

    const [isVisible, setVisible] = React.useState<boolean>(false);

    const [post, setPost] = React.useState<PostType>()

    const [file, setFile] = React.useState<null | Blob>();

    const [imgUrl, setImgUrl] = React.useState<string>("")

    const [edit, setEdit] = React.useState<boolean>(false)

    const [caption, setCaption] = React.useState<string>("")

    const handleCaptionChange = (event: any) => {
        setCaption(event.target.value)
    }

    const upload = async () => {
        try {
            const formData = new FormData()
            formData.append("file", file as Blob)
            const res = await axios.post("http://localhost:8800/api/upload", formData)
            return res.data
        } catch (err) {
            console.log(err)
        }
    }

    const mutationUpdatePost = useMutation({
        mutationFn: async (imgUrlParam: string) => {
            try {
                await axios.post("http://localhost:8800/api/posts/updatePost", {
                    caption: caption,
                    img: imgUrlParam,
                    id: post?.id
                })
            } catch (err) {
                console.log(err)
            }
            setImgUrl("")
            setCaption("")
            setFile(null)
            setVisible(false)
        },
        onSuccess: () => {
            queryClient.invalidateQueries( { queryKey: ['posts'] } ).then()
        },
    })

    const mutation = useMutation({
        mutationFn: async (imgUrlParam: string) => {
            try {
                await axios.post("http://localhost:8800/api/posts/addPost", {
                    userId: currentUser.id,
                    caption: caption,
                    img: imgUrlParam
                })
            } catch (err) {
                console.log(err)
            }
            setImgUrl("")
            setCaption("")
            setFile(null)
            setVisible(false)
        },
        onSuccess: () => {
            queryClient.invalidateQueries( { queryKey: ['posts'] } ).then()
        },
    })

    const handleAddPost = async () => {
        if (edit) {
            if (file) {
                const data = await upload()
                mutationUpdatePost.mutate(data || "")
            } else {
                mutationUpdatePost.mutate(imgUrl)
            }
        } else {
            if (file) {
                const data = await upload()
                mutation.mutate(data || "")
            } else {
                mutation.mutate(imgUrl)
            }
        }
    }

    React.useImperativeHandle(
        ref,
        () => ({
            onEdit: (post: PostType) => {
                setVisible(true)
                setEdit(true)
                setPost(post)
                setCaption(post.caption)
                setImgUrl(post.img)
            },
            open: () => setVisible(true),
            close: () => setVisible(false),
        }),
        [setVisible]
    )

    const getFile = (event: any) => {
        if(event.target.files && event.target.files.length > 0) {
            setFile(event.target.files[0])
        } else {
            setFile(null)
        }
    }

    const handleChangeColor = (_: Color, _hex: string) => {
        // setColor(hex);
    }

    return (
        <Modal
            onCancel={() => {
                setImgUrl("")
                setCaption("")
                setFile(null)
                setVisible(false)
            }}
            width={600}
            title={edit ? "Edit post" : "Create new post"}
            centered={true}
            open={isVisible}
            footer={false}
            closeIcon={<CloseIcon sx={{ color: "white" }}/>}
            destroyOnClose={true}
            className="border-none"
        >
            <div className="pb-4">
                <div className="mt-4 flex items-center mb-4">
                    <Avatar src={currentUser.profilePicture} sx={{ width: 32, height: 32 }}/>
                    <span className="text-white text-md">{currentUser.name}</span>
                </div>
                <TextArea onChange={handleCaptionChange} value={caption} rows={4} placeholder="What is on your mind"/>
                <img
                    className="w-full mt-4"
                    src={
                        !edit ?
                        file ?
                        URL.createObjectURL(file as Blob) :
                        "" :
                        file ?
                        URL.createObjectURL(file as Blob) :
                        "../../../public/upload/" + post?.img
                    }
                    alt=""
                />
                <div className="mt-4 px-4 flex justify-between">
                    <ColorPicker
                        defaultValue="#fff"
                        onChange={handleChangeColor}
                    />
                    <img className="w-8 h-8 hover:cursor-pointer hover:opacity-80" src={Emoji} alt="" />
                </div>
                <div className="flex justify-center items-center border-[1px] mx-4 mt-4 rounded-lg border-[#353638]">
                    <span className="text-white text-lg mr-4">Add to your post</span>
                    <img className="h-8 w-8 p-1 mx-4 my-2 cursor-pointer rounded-full hover:bg-[#36393f]" src={Camera} alt="" />
                    <label htmlFor="upload-file" className="relative hover:cursor-pointer">
                        <input
                            id="upload-file"
                            type="file"
                            onChange={getFile}
                            style={{ display: "none" }}
                        />
                        <img className="h-8 w-8 p-1 mx-4 my-2 cursor-pointer rounded-full hover:bg-[#36393f]"
                             src={Image}
                             alt=""/>
                    </label>
                    <img className="h-8 w-8 p-1 mx-4 my-2 cursor-pointer rounded-full hover:bg-[#36393f]" src={Attach} alt="" />
                    <img className="h-8 w-8 p-1 mx-4 my-2 cursor-pointer rounded-full hover:bg-[#36393f]" src={Location} alt="" />
                </div>
                <div className="flex justify-center">
                    <Button
                        onClick={handleAddPost}
                        className="w-[95%] h-10 mt-4 font-[500] text-white bg-[blue] border-none text-lg"
                    >
                        {edit ? "Save" : "Post"}
                    </Button>
                </div>
            </div>
        </Modal>
    );
};

export default React.forwardRef(UpPostModal);