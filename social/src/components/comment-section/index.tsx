import {Avatar} from "@mui/material";
import { Comment } from "../../types/type";
import { useNavigate } from "react-router-dom";

export interface CommentSectionProps {
    comment: Comment;
}

const CommentSection = (props: CommentSectionProps) => {

    const navigate = useNavigate()

    return (
        <div className="flex justify-start mt-3 mr-3 py-2">
            <Avatar onClick={() => navigate("/profile/" + props.comment.userId)} src={"/upload/" + props.comment.profilePicture} sx={{width: "30px", height: "30px", marginBottom: "0.5rem"}}/>
            <div className="bg-[#3a3b3c] max-w-[70%] rounded-lg py-1 px-3">
                <p className="text-white font-[500]">{props.comment.name}</p>
                <p className="text-white">{props.comment.comment}</p>
            </div>
        </div>
    )
}

export default CommentSection