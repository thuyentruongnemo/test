import React from "react";
import axios from "axios";
import { Profile } from "../types/type";
import { getUser } from "../redux/reducers/get-user.ts";
import { useDispatch, useSelector } from "react-redux";

export const AuthContext = React.createContext<any>(null);

export const AuthProvider = ({ children }: any) => {

  const dispatch = useDispatch();

  const state = useSelector((state: any) => state.getUser);

  let user = localStorage.getItem("user");
  const [currentUser, setCurrentUser] = React.useState<Profile>(
    user ? JSON.parse(user) : null
  );

  const login = async (user: any) => {
    await axios.post("http://localhost:8800/api/auth/login", user)
        .then((res) => {
          setCurrentUser(res.data);
        });
  }

  const fetchUser = async (id: number) => {
    const response = await axios.get('http://localhost:8800/api/users/find/' + id)
    setCurrentUser(response.data)
  }

  React.useEffect(() => {
    if(state.statusNeedToRefresh) {
      fetchUser(state.id).then(() => {
        dispatch(getUser({ id: state.id }))
      });
    }
  }, [state])

  React.useEffect(() => {
    localStorage.setItem("user", JSON.stringify(currentUser));
  }, [currentUser]);

  return (
    <AuthContext.Provider value={{ currentUser, login }}>
      {children}
    </AuthContext.Provider>
  );
};